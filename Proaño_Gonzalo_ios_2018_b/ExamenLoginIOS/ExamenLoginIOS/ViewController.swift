//
//  ViewController.swift
//  ExamenLoginIOS
//
//  Created by Gonzalo David Proaño on 27/11/18.
//  Copyright © 2018 Gonzalo David Proaño. All rights reserved.
//

import UIKit
import FirebaseAuth

class ViewController: UIViewController {

    @IBOutlet weak var userLaber: UITextField!
    @IBOutlet weak var passwordlabel: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    @IBAction func IngresarButtonPressed(_ sender: UIButton) {
        let user = userLaber.text!
        let pass = passwordlabel.text!
        
        
        Auth.auth().signIn(withEmail: user, password: pass) {
            (data,error) in
            if let error = error {
                print (error)
                return
            }
            //print("Welcome....!!!!")
            self.performSegue(withIdentifier: "LoginNumber", sender: self)
        }
        
    }

}

